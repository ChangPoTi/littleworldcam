# README #

Little world cam is a fun ios app which can transform your photo into a circle and it will make your photo looks like a tiny planet. LWC has great image processing performance which takes advantage of GPU. User can save the image or share the image to social network. LWC also includes iAd.

### Screen shot ###
![iOS Simulator Screen Shot 2014年12月30日 下午5.42.02.png](https://bitbucket.org/repo/MXjk4k/images/2295440575-iOS%20Simulator%20Screen%20Shot%202014%E5%B9%B412%E6%9C%8830%E6%97%A5%20%E4%B8%8B%E5%8D%885.42.02.png)
![iOS Simulator Screen Shot 2014年12月30日 下午5.42.08.png](https://bitbucket.org/repo/MXjk4k/images/2475624357-iOS%20Simulator%20Screen%20Shot%202014%E5%B9%B412%E6%9C%8830%E6%97%A5%20%E4%B8%8B%E5%8D%885.42.08.png)
![iOS Simulator Screen Shot 2014年12月30日 下午5.42.30.png](https://bitbucket.org/repo/MXjk4k/images/1219575044-iOS%20Simulator%20Screen%20Shot%202014%E5%B9%B412%E6%9C%8830%E6%97%A5%20%E4%B8%8B%E5%8D%885.42.30.png)

### Features ###

* Fast image processing performance with GPU
* iAd
* Share to social network