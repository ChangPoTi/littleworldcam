//
//  PlanetViewController.m
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/23.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import "PlanetViewController.h"
#import "ImageScrollView.h"
#import "LittleGalaxyAlbumData.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface PlanetViewController ()
@property (weak, nonatomic) IBOutlet ImageScrollView *imageScrollView;
@end

@implementation PlanetViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.displayedImage)
    {
        NSArray *assets = [LittleGalaxyAlbumData sharedInstance].assets;
        if (self.index < [assets count])
        {
            ALAsset *photoAsset = [assets objectAtIndex:self.index];

            ALAssetRepresentation *assetRepresentation = [photoAsset defaultRepresentation];
            
            UIImage *fullScreenImage = [UIImage imageWithCGImage:[assetRepresentation fullResolutionImage]
                                                           scale:[assetRepresentation scale]
                                                     orientation:UIImageOrientationUp];
            self.displayedImage = fullScreenImage;
        }
    }
    self.imageScrollView.image = self.displayedImage;
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapGesture:)];
    doubleTap.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTap];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesture:)];
    [singleTap requireGestureRecognizerToFail:doubleTap];
    [self.view addGestureRecognizer:singleTap];
}

- (void)setDisplayedImage:(UIImage *)displayedImage
{
    _displayedImage = displayedImage;
    if (self.imageScrollView)
        self.imageScrollView.image = displayedImage;
}

- (void)handleSingleTapGesture:(UITapGestureRecognizer*)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateRecognized)
    {
        [self.navigationController setNavigationBarHidden:!self.navigationController.isNavigationBarHidden animated:YES];
    }
}

- (void)handleDoubleTapGesture:(UITapGestureRecognizer*)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateRecognized)
    {
        if (self.imageScrollView.zoomScale > self.imageScrollView.minimumZoomScale)
            [self.imageScrollView setZoomScale:self.imageScrollView.minimumZoomScale animated:YES];
        else
            [self.imageScrollView setZoomScale:self.imageScrollView.maximumZoomScale animated:YES];
    }
}

- (IBAction)onAction:(UIBarButtonItem *)sender
{
    NSArray *items = @[self.displayedImage];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeSaveToCameraRoll];
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

@end
