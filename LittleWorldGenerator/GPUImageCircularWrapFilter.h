//
//  GPUImageCircularWrap.h
//  GPUImageCustomFilter
//
//  Created by Birdy Chang on 2014/2/24.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import "GPUImageFilter.h"

@interface GPUImageCircularWrapFilter : GPUImageFilter
{
    GLint wrapInUniform;
}
@property (nonatomic) bool wrapIn;
@end
