//
//  LittleGalaxyAlbumData.h
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/27.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <Foundation/Foundation.h>
extern NSString *const LittleGalaxyAlbumDataUpdated;

@interface LittleGalaxyAlbumData : NSObject
@property (strong, nonatomic, readonly) NSArray *assets;
+ (instancetype)sharedInstance;
@end
