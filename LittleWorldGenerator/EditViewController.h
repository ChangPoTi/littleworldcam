//
//  EditViewController.h
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 14/1/11.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditViewController : UIViewController

@property (strong, nonatomic) UIImage* sourceImage;

@end
