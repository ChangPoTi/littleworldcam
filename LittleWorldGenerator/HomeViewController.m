//
//  ViewController.m
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 14/1/4.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import "HomeViewController.h"
#import "EditViewController.h"

#define EDITVIEW_SEG_ID @"edit view"

@interface HomeViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (strong, nonatomic) UIImage* targetImage;
@end

@implementation HomeViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (IBAction)onLibrary:(UIButton *)sender
{
    [self createImagePicker:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (IBAction)onTakePhoto:(UIButton *)sender
{
    [self createImagePicker:UIImagePickerControllerSourceTypeCamera];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:EDITVIEW_SEG_ID]) {
        if ([segue.destinationViewController isKindOfClass:[EditViewController class]]) {
            EditViewController *view = (EditViewController *)segue.destinationViewController;
            if (self.targetImage) view.sourceImage = self.targetImage;
        }
    }
}

- (void)createImagePicker:(UIImagePickerControllerSourceType)type
{
    if ([UIImagePickerController isSourceTypeAvailable:type]) {
        NSArray *availableTypes = [UIImagePickerController availableMediaTypesForSourceType:type];
        if ([availableTypes containsObject:(NSString*)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = type;
            picker.mediaTypes = @[(NSString*)kUTTypeImage];
            picker.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
            picker.navigationBar.titleTextAttributes = self.navigationController.navigationBar.titleTextAttributes;
            picker.navigationBar.barTintColor = self.navigationController.navigationBar.barTintColor;
            picker.allowsEditing = YES;
            picker.delegate = self;
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
    else
        NSLog(@"%ld is inavailable", type);
}

#pragma mark Delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = nil;
    UIImage *editImage = info[UIImagePickerControllerEditedImage];
    UIImage *oriImage = info[UIImagePickerControllerOriginalImage];
    
    if (editImage)
    {
        CGRect crop = [info[UIImagePickerControllerCropRect] CGRectValue];
        CGImageRef cropImage = CGImageCreateWithImageInRect(oriImage.CGImage, crop);
        image = [UIImage imageWithCGImage:cropImage];
        CGImageRelease(cropImage);
    }
    else
        image = oriImage;

    if (image) {
        self.targetImage = image;
        [self performSegueWithIdentifier:EDITVIEW_SEG_ID sender:self];
    }
    else
        self.targetImage = nil;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
