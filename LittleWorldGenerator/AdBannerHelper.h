//
//  AdBannerHelper.h
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/3/8.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <iAd/iAd.h>

@interface AdBannerHelper : NSObject <ADBannerViewDelegate>
@property (weak, nonatomic) ADBannerView *adBannerView;
@property (weak, nonatomic) NSLayoutConstraint *adBottomConstraint;
- (void)setAdBannerVisiable:(BOOL)visiable;
@end
