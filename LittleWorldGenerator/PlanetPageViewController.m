//
//  PlanetPageViewController.m
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/24.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import "PlanetPageViewController.h"
#import "PlanetViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "LittleGalaxyAlbumData.h"


@interface PlanetPageViewController () <UIPageViewControllerDataSource>
@end

@implementation PlanetPageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.dataSource = self;

    PlanetViewController *startVc = [self getPlanetViewControllerAtIndex:self.startIndex];
    if (startVc)
        [self setViewControllers:@[startVc] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(assetsIsUpdated) name:LittleGalaxyAlbumDataUpdated object:[LittleGalaxyAlbumData sharedInstance]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)assetsIsUpdated
{
    NSLog(@"[%@] assetsIsUpdated...", [self class]);
    if (!self.viewControllers)
        return;
    
    NSArray *assets = [LittleGalaxyAlbumData sharedInstance].assets;
    PlanetViewController *curVc = (PlanetViewController*)[self.viewControllers objectAtIndex:0];
    NSUInteger curIdx = curVc.index;
    NSUInteger newIdx = ([assets count] > curIdx) ? curIdx : [assets count] - 1;
    PlanetViewController *newVc = [self getPlanetViewControllerAtIndex:newIdx];
    if (newVc)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setViewControllers:@[newVc]
                           direction:UIPageViewControllerNavigationDirectionReverse animated:(curIdx != newIdx) completion:nil];
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController popToRootViewControllerAnimated:YES];
        });
    }
}

- (PlanetViewController*)getPlanetViewControllerAtIndex:(NSUInteger)index
{
    if (index < [[LittleGalaxyAlbumData sharedInstance].assets count])
    {
        PlanetViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PlanetViewController"];
        vc.index = index;
        return vc;
    }
    else
        return nil;
}

- (IBAction)onAction:(UIBarButtonItem *)sender
{
    if (!self.viewControllers)
        return;
    
    PlanetViewController *curVc = (PlanetViewController*)[self.viewControllers objectAtIndex:0];
    [curVc onAction:sender];
}

#pragma mark - UIPageViewControllerDataSource methods

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    PlanetViewController *currentVc = (PlanetViewController*)viewController;
    PlanetViewController *vc = [self getPlanetViewControllerAtIndex:currentVc.index - 1];

    return vc;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    PlanetViewController *currentVc = (PlanetViewController*)viewController;
    PlanetViewController *vc = [self getPlanetViewControllerAtIndex:currentVc.index + 1];

    return vc;
}

@end
