//
//  BrowseViewCell.h
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/22.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowseViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end
