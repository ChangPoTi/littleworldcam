//
//  ALAssetsLibrary+GetAssetsInAlbum.m
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/24.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import "ALAssetsLibrary+GetAssetsInAlbum.h"

@implementation ALAssetsLibrary (GetAssetsInAlbum)

- (void)getAssetsInAlbum:(NSString *)albumName withFilter:(ALAssetsFilter*)filter complete:(AssetsInAlbumCompleteBlock)completeBlock failure:(AssetsInAlbumFailureBlock)failureBlock
{
    [self enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop){
        if ([albumName compare: [group valueForProperty:ALAssetsGroupPropertyName]] == NSOrderedSame)
        {

            [group setAssetsFilter:filter];
            *stop = YES;
            
            NSMutableArray *assets = [[NSMutableArray alloc] init];
            [group enumerateAssetsUsingBlock:^(ALAsset *asset, NSUInteger index, BOOL *stop){
                if (asset)
                {
                    [assets addObject:asset];
                }
                else
                {
                    if (completeBlock)
                        completeBlock(assets);
                }
            }];
        }
        if (!*stop && !group && completeBlock)
            completeBlock(nil);
        
    } failureBlock:^(NSError *error){
        if (error)
            NSLog(@"[ALAssetsLibrary+GetAssetsInAlbum] getAssetsInAlbum, error:%@", [error description]);
        if (failureBlock)
            failureBlock(error);
    }];
}

@end
