//
//  UIImage+Resize.h
//  FilterGallery
//
//  Created by Birdy Chang on 2014/12/18.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)
- (UIImage*)imageScaledToSize:(CGSize)size;
- (UIImage*)imageScaledToFitSize:(CGSize)size;
@end
