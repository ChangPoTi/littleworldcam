//
//  ALAssetsLibrary+GetAssetsInAlbum.h
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/24.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

typedef void(^AssetsInAlbumCompleteBlock)(NSArray *assets);
typedef void(^AssetsInAlbumFailureBlock)(NSError *error);

@interface ALAssetsLibrary (GetAssetsInAlbum)

- (void)getAssetsInAlbum:(NSString*)albumName withFilter:(ALAssetsFilter*)filter complete:(AssetsInAlbumCompleteBlock)completeBlock failure:(AssetsInAlbumFailureBlock)failureBlock;

@end
