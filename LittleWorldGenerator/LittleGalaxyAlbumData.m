//
//  LittleGalaxyAlbumData.m
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/27.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import "LittleGalaxyAlbumData.h"
#import "ALAssetsLibrary+GetAssetsInAlbum.h"
#import "GlobalDef.h"


NSString *const LittleGalaxyAlbumDataUpdated = @"LittleGalaxyAlbumDataUpdated";

@interface LittleGalaxyAlbumData()
@property (strong, nonatomic) ALAssetsLibrary *assetsLibrary;
@property (strong, nonatomic) NSArray *assets;
@end

@implementation LittleGalaxyAlbumData

- (ALAssetsLibrary *)assetsLibrary
{
    if (!_assetsLibrary)
        _assetsLibrary = [[ALAssetsLibrary alloc] init];
    return _assetsLibrary;
}

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static LittleGalaxyAlbumData *sSharedInstance;
    
    dispatch_once(&onceToken, ^{
        sSharedInstance = [[LittleGalaxyAlbumData alloc] init];
    });
    return sSharedInstance;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        [self updateItemsInAlbum:[[NSUserDefaults standardUserDefaults] stringForKey:PREF_ALBUM_KEY]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(assetsLibraryChanged:) name:ALAssetsLibraryChangedNotification object:self.assetsLibrary];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)assetsLibraryChanged:(NSNotification *)notification
{
    NSLog(@"[%@] assetsLibrarychnaged...", [self class]);
    NSDictionary *userInfo = [notification userInfo];
    
    if (userInfo || userInfo == nil)
    {
        if ([userInfo valueForKey:ALAssetLibraryUpdatedAssetsKey])
            [self updateItemsInAlbum:[[NSUserDefaults standardUserDefaults] stringForKey:PREF_ALBUM_KEY]];
    }
}

- (void)updateItemsInAlbum:(NSString *)albumName
{
    [self.assetsLibrary getAssetsInAlbum:albumName withFilter:[ALAssetsFilter allPhotos]
                                complete:^(NSArray *assets){
                                    self.assets = assets;
                                    [[NSNotificationCenter defaultCenter] postNotificationName:LittleGalaxyAlbumDataUpdated object:[LittleGalaxyAlbumData sharedInstance]];
                                }
                                 failure:nil];
}

@end
