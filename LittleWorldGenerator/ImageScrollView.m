//
//  ImageScrollView.m
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/28.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import "ImageScrollView.h"

@interface ImageScrollView() <UIScrollViewDelegate>
@property (strong, nonatomic) UIImageView *imageView;
@end

@implementation ImageScrollView

- (void)setImage:(UIImage *)image
{
    _image = image;
    
    self.imageView.image = image;
    self.imageView.bounds = CGRectMake(0, 0, image.size.width, image.size.height);

    self.minimumZoomScale = self.bounds.size.width / image.size.width;
    [self setZoomScale:self.minimumZoomScale animated:NO];
}

- (UIImageView *)imageView
{
    if (!_imageView)
    {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    }
    return _imageView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    self.delegate = self;
    self.maximumZoomScale = 1.5;
    
    [self addSubview:self.imageView];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // center the zoom view as it becomes smaller than the size of the screen
    CGSize boundsSize = self.bounds.size;
    CGRect frameToCenter = self.imageView.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    else
        frameToCenter.origin.y = 0;
    
    self.imageView.frame = frameToCenter;
}

- (void)dealloc
{
    self.delegate = nil;
}

#pragma mark - UIScrollViewDelgate methods
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

@end
