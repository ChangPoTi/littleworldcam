//
//  FilterPreviewCell.m
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/12/27.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import "FilterPreviewCell.h"
@interface FilterPreviewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *iv;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation FilterPreviewCell

- (void)setImage:(UIImage *)image {
    if (_image != image) {
        _image = image;
        self.iv.image = _image;
    }
}

- (void)setName:(NSString *)name {
    if (_name != name) {
        _name = name;
        self.label.text = _name;
    }
}

@end
