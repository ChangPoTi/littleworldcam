//
//  FilterPreviewViewController.h
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/12/27.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"

@class FilterPreviewViewController;

@protocol FilterPreviewViewControllerDelegate <NSObject>
- (void)filterPreviewDidCancel:(FilterPreviewViewController*)viewController;
- (void)filterPreviewDidSelect:(FilterPreviewViewController*)viewController withFilter:(GPUImageFilter*)filter;
- (UIImage*)filteringImage:(UIImage*)image withSize:(CGSize)size filter:(GPUImageFilter*)filter;
@end

@interface FilterPreviewViewController : UIViewController
@property (weak, nonatomic) id<FilterPreviewViewControllerDelegate> delegate;
@property (strong, nonatomic) UIImage *source;
@end
