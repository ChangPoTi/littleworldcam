//
//  ImageScrollView.h
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/28.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageScrollView : UIScrollView
@property (strong, nonatomic) UIImage *image;
@end
