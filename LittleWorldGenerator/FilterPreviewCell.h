//
//  FilterPreviewCell.h
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/12/27.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterPreviewCell : UICollectionViewCell
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *name;
@end
