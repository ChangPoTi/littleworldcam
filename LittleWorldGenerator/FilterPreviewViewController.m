//
//  FilterPreviewViewController.m
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/12/27.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import "FilterPreviewViewController.h"
#import "FilterPreviewCell.h"

@interface FilterPreviewViewController () <UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *filterCV;
@property (strong, nonatomic) NSMutableDictionary *previews;
@property (strong, nonatomic) NSArray *filters;
@property (strong, nonatomic) NSArray *filterNames;
@property (strong, nonatomic, readonly) NSOperationQueue *opQueue;
@end

@implementation FilterPreviewViewController

@synthesize opQueue = _opQueue;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.filterCV.dataSource = self;
    self.filterCV.delegate = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)dealloc
{
    self.filterCV.dataSource = nil;
    self.filterCV.delegate = nil;
}

#pragma mark Properties

- (NSMutableDictionary *)previews {
    if (!_previews) {
        _previews = [[NSMutableDictionary alloc] init];
    }
    return _previews;
}

- (NSArray *)filters {
    if (!_filters) {
        _filters = @[[[GPUImagePixellateFilter alloc] init], [[GPUImagePolkaDotFilter alloc] init], [[GPUImageHalftoneFilter alloc] init], [[GPUImageSketchFilter alloc] init], [[GPUImageToonFilter alloc] init], [[GPUImageEmbossFilter alloc] init], [[GPUImagePosterizeFilter alloc] init], [[GPUImageErosionFilter alloc] init]];
    }
    return _filters;
}

- (NSArray *)filterNames {
    if (!_filterNames) {
        _filterNames = @[NSLocalizedString(@"None", nil), NSLocalizedString(@"Pixellate", nil), NSLocalizedString(@"Polka Dot", nil), NSLocalizedString(@"Halftone", nil), NSLocalizedString(@"Sketch", nil), NSLocalizedString(@"Toon", nil), NSLocalizedString(@"Emboss", nil), NSLocalizedString(@"Posterize", nil), NSLocalizedString(@"Erosion", nil)];
    }
    return _filterNames;
}

- (NSOperationQueue *)opQueue {
    if (!_opQueue) {
        _opQueue = [[NSOperationQueue alloc] init];
    }
    return _opQueue;
}

- (void)setSource:(UIImage *)source {
    if (_source != source) {
        _source = source;

    }
}

#pragma mark Private methods


#pragma mark Delegates

#define PROCESS_SIZE 120

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger index = [indexPath indexAtPosition:1];
    
    FilterPreviewCell *cell = (FilterPreviewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"filter_cell" forIndexPath:indexPath];
    UIImage *preview = [self.previews objectForKey:[NSNumber numberWithInteger:index]];
    
    if (preview) {
        cell.image = preview;
    }
    else {
        if (self.source && [self.delegate respondsToSelector:@selector(filteringImage:withSize:filter:)]) {
            NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^{
                GPUImageFilter *filter = (index == 0) ? nil : [self.filters objectAtIndex:index - 1];
                UIImage *result = [self.delegate filteringImage:self.source withSize:CGSizeMake(PROCESS_SIZE, PROCESS_SIZE) filter:filter];
                
                if (result)
                    [self.previews setObject:result forKey:[NSNumber numberWithInteger:index]];
            }];
            
            op.completionBlock = ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.filterCV reloadItemsAtIndexPaths:@[indexPath]];
                });
            };
            
            [self.opQueue addOperation:op];
        }
    }
        
    cell.name = [self.filterNames objectAtIndex:index];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return (section == 0) ? self.filterNames.count : 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = [indexPath indexAtPosition:1];
    GPUImageFilter *filter = (index > 0) ? [self.filters objectAtIndex:index - 1] : nil;
    [self.delegate filterPreviewDidSelect:self withFilter:filter];
}

#pragma mark Actions

- (IBAction)onCancel:(UIBarButtonItem *)sender {
    [self.delegate filterPreviewDidCancel:self];
}

@end
