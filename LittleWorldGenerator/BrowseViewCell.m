//
//  BrowseViewCell.m
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/22.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import "BrowseViewCell.h"

@implementation BrowseViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    self.clipsToBounds = YES;
    self.layer.cornerRadius = self.bounds.size.width / 2;
}

@end
