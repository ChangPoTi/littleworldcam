//
//  AppDelegate.h
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 14/1/4.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
