//
//  GPUImageCircularWrap.m
//  GPUImageCustomFilter
//
//  Created by Birdy Chang on 2014/2/24.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import "GPUImageCircularWrapFilter.h"
#import "GPUImageTransformFilter.h"

NSString *const kGPUImageCircularWrapFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 uniform sampler2D inputImageTexture;
 uniform lowp int wrapIn;
 
 void main()
 {
     highp float PI = 3.14159265358979323846264;
     
     highp vec2 norCoordinate = 2.0 * (textureCoordinate - 0.5);
     highp float r = length(norCoordinate);
     
     if (r > 1.0)
         gl_FragColor = vec4(1.0, 1.0, 1.0, 0.0);
     else
     {
         highp float phi = atan(norCoordinate.y, norCoordinate.x) / (2.0 * PI) - 0.25;
         phi = mod(phi, 1.0);

         if (phi < 0.0) phi += 1.0;
         phi *= 0.9;
         
         if (wrapIn == 0) r = 1.0 - r;
         
         if (phi < 0.1)
         {
             mediump float d = phi / 0.1;
             highp vec4 mixColor = texture2D(inputImageTexture, vec2(0.9 + phi, r));
             highp vec4 oriColor = texture2D(inputImageTexture, vec2(phi, r));
             gl_FragColor = oriColor * d + mixColor * (1.0 - d);
         }
         else
             gl_FragColor = texture2D(inputImageTexture, vec2(phi, r));
     }
 }
);

@implementation GPUImageCircularWrapFilter

- (id)init
{
    if (self = [super initWithFragmentShaderFromString:kGPUImageCircularWrapFragmentShaderString])
    {
        wrapInUniform = [filterProgram uniformIndex:@"wrapIn"];
        
        self.wrapIn = NO;
    }
    return self;
}

- (void)setWrapIn:(bool)wrapIn
{
    _wrapIn = wrapIn;
    
    [self setInteger:(wrapIn) ? 1 : 0 forUniform:wrapInUniform program:filterProgram];
}

@end
