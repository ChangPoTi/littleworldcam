//
//  GlobalDef.h
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/2/7.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#ifndef LittleWorldGenerator_GlobalDef_h
#define LittleWorldGenerator_GlobalDef_h

#define PREF_ALBUM_KEY @"Album Name"
#define PREF_MAX_OUTPUT_KEY @"Max Output Size"

#define STORY_ID_HOME @"HomeViewController"
#define STORY_ID_PLANET @"PlanetViewController"

#endif
