//
//  AdBannerHelper.m
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/3/8.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import "AdBannerHelper.h"

@implementation AdBannerHelper

- (void)setAdBannerView:(ADBannerView *)adBannerView
{
    if (adBannerView)
    {
        _adBannerView = adBannerView;
        adBannerView.delegate = self;
    }
}

- (void)setAdBannerVisiable:(BOOL)visiable
{
    [self.adBannerView.superview layoutIfNeeded];
    [UIView animateWithDuration:0.5 animations:^{
        self.adBottomConstraint.constant = (visiable) ? 0 : -self.adBannerView.frame.size.height;
        [self.adBannerView.superview layoutIfNeeded];
    }];
}

- (void)dealloc
{
    self.adBannerView.delegate = nil;
}

#pragma mark ADBannerViewDelegate methods

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    [self setAdBannerVisiable:YES];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    if (error)
        NSLog(@"didFailToReceiveAdWithError:%@", [error description]);
    
    [self setAdBannerVisiable:NO];
}

@end
