//
//  PlanetViewController.h
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/23.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlanetViewController : UIViewController
@property (nonatomic) NSInteger index;
@property (strong, nonatomic) UIImage *displayedImage;
- (IBAction)onAction:(UIBarButtonItem *)sender;
@end
