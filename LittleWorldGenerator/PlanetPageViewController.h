//
//  PlanetPageViewController.h
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/24.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlanetPageViewController : UIPageViewController
@property (nonatomic) NSUInteger startIndex;
@end
