//
//  BrowseViewController.m
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 2014/1/22.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import "BrowseViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "BrowseViewCell.h"
#import "PlanetPageViewController.h"
#import "LittleGalaxyAlbumData.h"
#import <iAd/iAd.h>
#import "AdBannerHelper.h"

@interface BrowseViewController () <UICollectionViewDelegate, UICollectionViewDataSource>
{
    AdBannerHelper *_adBannerHelper;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet ADBannerView *adBannerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *adBottomContraint;
@property (nonatomic) NSInteger selectedIndex;
@end

@implementation BrowseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor =  [UIColor clearColor];
    
    _adBannerHelper = [[AdBannerHelper alloc] init];
    _adBannerHelper.adBannerView = self.adBannerView;
    _adBannerHelper.adBottomConstraint = self.adBottomContraint;
    [_adBannerHelper setAdBannerVisiable:self.adBannerView.bannerLoaded];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;

    [self.collectionView reloadData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(assetsIsUpdated) name:LittleGalaxyAlbumDataUpdated object:[LittleGalaxyAlbumData sharedInstance]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)assetsIsUpdated
{
    NSLog(@"[%@] assetsIsUpdated...", [self class]);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"planet view"] && [segue.destinationViewController isKindOfClass:[PlanetPageViewController class]])
    {
        PlanetPageViewController *vc = (PlanetPageViewController*)segue.destinationViewController;
        vc.startIndex = self.selectedIndex;
    }
}

- (void)dealloc
{
    self.collectionView.delegate = nil;
    self.collectionView.dataSource = nil;
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSArray *items = [LittleGalaxyAlbumData sharedInstance].assets;
    return (items) ? [items count] : 0;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath length ] == 2)
    {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"planet" forIndexPath:indexPath];
        if ([cell isKindOfClass:[BrowseViewCell class]])
        {
            BrowseViewCell *bCell = (BrowseViewCell*)cell;
            NSArray *items = [LittleGalaxyAlbumData sharedInstance].assets;
            NSUInteger index = [indexPath indexAtPosition:1];
            ALAsset *asset = (index < [items count]) ? (ALAsset*)[items objectAtIndex:index] : nil;
            bCell.imageView.image = [UIImage imageWithCGImage:[asset thumbnail]];
        }
        return cell;
    }
    else
        return nil;
}

#pragma mark - UICollectionViewDelegate methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath length] == 2)
    {
        self.selectedIndex = [indexPath indexAtPosition:1];
        [self performSegueWithIdentifier:@"planet view" sender:self];
    }
}

@end
