//
//  EditViewController.m
//  LittleWorldGenerator
//
//  Created by Birdy Chang on 14/1/11.
//  Copyright (c) 2014年 Birdy Chang. All rights reserved.
//

#import <iAd/iAd.h>
#import "EditViewController.h"
#import "MBProgressHUD.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "GlobalDef.h"
#import "GPUImage.h"
#import "GPUImageCircularWrapFilter.h"
#import "AdBannerHelper.h"
#import "PlanetViewController.h"
#import "FilterPreviewViewController.h"
#import "UIImage+Resize.h"

@interface EditViewController () <FilterPreviewViewControllerDelegate>
{
    AdBannerHelper *_adBannerHelper;
    BOOL _isSaving;
}

@property (weak, nonatomic) IBOutlet UIView *frameView;
@property (weak, nonatomic) IBOutlet ADBannerView *adBannerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *adBottomConstraint;

@property (strong, nonatomic) UIImageView *previewIV;
@property (nonatomic) BOOL isSquare;
@property (nonatomic) BOOL isPlanet;
@property (nonatomic) CGFloat rotation;
@property (strong, nonatomic) ALAssetsLibrary *assetLibrary;
@property (strong, nonatomic) GPUImageFilter *applyFilter;
@end

@implementation EditViewController

#pragma mark Properties

- (UIImageView*)previewIV
{
    if (!_previewIV)
    {
        _previewIV = [[UIImageView alloc] initWithFrame:CGRectZero];
        _previewIV.opaque = NO;
        _previewIV.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _previewIV;
}

- (void)setSourceImage:(UIImage *)sourceImage
{
    if (sourceImage)
    {
        _sourceImage = sourceImage;
        [self resetPreview];
    }
}

- (void)setIsPlanet:(BOOL)isPlanet
{
    if (_isPlanet != isPlanet)
    {
        _isPlanet = isPlanet;
        [self resetPreview];
    }
}

- (void)setIsSquare:(BOOL)isSquare
{
    if (_isSquare != isSquare)
    {
        _isSquare = isSquare;

        [UIView animateWithDuration:0.3 animations:^{
            CGFloat scale = 1 / sinf(M_PI_4);
            self.previewIV.transform = (isSquare) ? CGAffineTransformScale(self.previewIV.transform, scale, scale) : CGAffineTransformConcat(self.previewIV.transform, CGAffineTransformInvert(CGAffineTransformMakeScale(scale, scale)));
        }];
    }
}

- (void)setRotation:(CGFloat)rotation
{
    CGFloat delta = _rotation - rotation;
    self.previewIV.transform = CGAffineTransformRotate(self.previewIV.transform, delta);
    _rotation = rotation;
}

- (ALAssetsLibrary *)assetLibrary
{
    if (!_assetLibrary)
        _assetLibrary = [[ALAssetsLibrary alloc] init];
    return _assetLibrary;
}

#pragma mark ViewController methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isPlanet = YES;

    NSLayoutConstraint *newCon = [NSLayoutConstraint constraintWithItem:self.frameView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.frameView attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0];
    newCon.priority = UILayoutPriorityRequired;
    
    [self.frameView addConstraint:newCon];
    [self.frameView addSubview:self.previewIV];
    
    _adBannerHelper = [[AdBannerHelper alloc] init];
    _adBannerHelper.adBannerView = self.adBannerView;
    _adBannerHelper.adBottomConstraint = self.adBottomConstraint;
    [_adBannerHelper setAdBannerVisiable:self.adBannerView.bannerLoaded];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self resetPreview];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    self.previewIV.bounds = self.frameView.bounds;
    self.previewIV.center = CGPointMake(CGRectGetMidX(self.frameView.bounds), CGRectGetMidY(self.frameView.bounds));
}

#define FILTER_PREVIEW_SIZE 120

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"filters_preview"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        FilterPreviewViewController *vc = [navigationController viewControllers][0];
        vc.source = [self.sourceImage imageScaledToFitSize:CGSizeMake(FILTER_PREVIEW_SIZE, FILTER_PREVIEW_SIZE)];
        vc.delegate = self;
    }
}

#pragma mark Private methods

- (void)resetPreview
{
    if (!self.isViewLoaded)
        return;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = NSLocalizedString(@"Processing", nil);
    
    CGSize size = CGSizeApplyAffineTransform(self.previewIV.bounds.size, CGAffineTransformMakeScale([UIScreen mainScreen].scale, [UIScreen mainScreen].scale));
    
    GPUImageCircularWrapFilter *wrapFilter = [[GPUImageCircularWrapFilter alloc] init];
    wrapFilter.wrapIn = !self.isPlanet;
    [wrapFilter forceProcessingAtSize:size];
    
    GPUImagePicture *pic = [[GPUImagePicture alloc] initWithImage:self.sourceImage];
    [pic addTarget:wrapFilter];
    GPUImageFilter *lastFilter = wrapFilter;
    
    if (self.applyFilter) {
        [lastFilter addTarget:self.applyFilter];
        lastFilter = self.applyFilter;
    }
    
    [pic processImageUpToFilter:lastFilter withCompletionHandler:^(UIImage* processedImage){
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hide:YES afterDelay:0.3];
            self.previewIV.image = processedImage;
        });
    }];
}

- (void)processOutputWithCompletionBlock:(void (^)(UIImage*))complete
{
    CGFloat sin_PI_4 = sinf(M_PI_4);
    CGFloat outputSize = MIN(MIN(self.sourceImage.size.width, self.sourceImage.size.height), [[NSUserDefaults standardUserDefaults] floatForKey:PREF_MAX_OUTPUT_KEY]);
    
    outputSize = (self.isSquare) ? outputSize / sin_PI_4 : outputSize;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *output = [self filteringImage:self.sourceImage withSize:CGSizeMake(outputSize, outputSize) filter:self.applyFilter];
        complete(output);
    });
}

- (UIImage*)filteringImage:(UIImage*)image withSize:(CGSize)size filter:(GPUImageFilter*)filter {

    GPUImageFilter *initFilter, *lastFilter;
        
    GPUImageCircularWrapFilter *wrapFilter = [[GPUImageCircularWrapFilter alloc] init];
    [wrapFilter forceProcessingAtSize:size];
    wrapFilter.wrapIn = !self.isPlanet;
    initFilter = lastFilter = wrapFilter;
        
    if (filter) {
        [lastFilter addTarget:filter];
        lastFilter = filter;
    }
    
    GPUImageTransformFilter *transFilter = [[GPUImageTransformFilter alloc] init];
    transFilter.affineTransform = CGAffineTransformMakeRotation(-self.rotation);
    [lastFilter addTarget:transFilter];
    lastFilter = transFilter;
    
    if (self.isSquare)
    {
        CGFloat sin_PI_4 = sinf(M_PI_4);
        GPUImageCropFilter *cropFilter = [[GPUImageCropFilter alloc] init];
        CGFloat ori = 0.5 * (1.0 - sin_PI_4);
        CGFloat size = sin_PI_4;
        cropFilter.cropRegion = CGRectMake(ori, ori, size, size);
        [lastFilter addTarget:cropFilter];
        lastFilter = cropFilter;
    }
    
    GPUImagePicture *pic = [[GPUImagePicture alloc] initWithImage:image];
    [pic addTarget:initFilter];
    [lastFilter useNextFrameForImageCapture];
    [pic processImage];
    
    return [lastFilter imageFromCurrentFramebuffer];
}

- (void)jumpToSavedImage:(UIImage*)savedImage
{
    UIViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:STORY_ID_HOME];
    
    PlanetViewController *planet = [self.storyboard instantiateViewControllerWithIdentifier:STORY_ID_PLANET];
    planet.displayedImage = savedImage;
    
    [self.navigationController setViewControllers:@[home, planet] animated:YES];
    
}

#pragma mark Actions

- (IBAction)onSquare:(UIButton *)sender
{
    [sender setSelected:!sender.selected];
    self.isSquare = sender.selected;
}

- (IBAction)onPlanet:(UIButton *)sender
{
    [sender setSelected:!sender.selected];
    self.isPlanet = !sender.selected;
}

- (IBAction)onSave:(UIBarButtonItem *)sender
{
    if (_isSaving) return;
    
    _isSaving = true;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Saving", nil);
    
    [self processOutputWithCompletionBlock:^(UIImage *output){
        
        NSString *albumName = [[NSUserDefaults standardUserDefaults] stringForKey:PREF_ALBUM_KEY];
        
        [self.assetLibrary saveImage:output asType:PNG toAlbum:albumName withCompletionBlock:^(NSError *error){
            if (error) NSLog(@"error:%@", [error description]);
        }];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            _isSaving = false;
            [hud hide:YES afterDelay:0.3];
        
            [self jumpToSavedImage:output];
        });
    }];
}

- (IBAction)onRotate:(UIRotationGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan || sender.state == UIGestureRecognizerStateChanged)
    {
        self.rotation -= sender.rotation;
        sender.rotation = 0;
    }
}

#pragma mark Delegates

- (void)filterPreviewDidCancel:(FilterPreviewViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)filterPreviewDidSelect:(FilterPreviewViewController *)viewController withFilter:(GPUImageFilter *)filter {
    [self dismissViewControllerAnimated:YES completion:nil];
    self.applyFilter = filter;
}

@end
